const knex = require('../knex')
const paypal = require('../paypal')

module.exports = {
  payment: async (req, res) => {
    const orders =
      await knex.from('orders as o')
        .innerJoin('order_products as op', 'o.id', '=', 'op.order_id')
        .innerJoin('products as p', 'p.id', '=', 'op.product_id')
        .select('o.*')
        .select('op.price', 'op.amount')
        .select('p.name', 'p.unity')
        .where('o.id', '=', req.params.order_id)

    const items = orders.map(order => {
      return {
        name: order.name,
        sku: order.id,
        price: order.price,
        currency: 'BRL',
        quantity: order.amount
      }
    })

    const createPaymentJson = {
      intent: 'sale',
      payer: {
        payment_method: 'paypal'
      },
      redirect_urls: {
        return_url: process.env.URL + '/success/' + orders[0].total,
        cancel_url: process.env.URL + '/cancel'
      },
      transactions: [
        {
          item_list: {
            items
          },
          amount: {
            currency: 'BRL',
            total: orders[0].total
          },
          description: 'This is the payment description.'
        }
      ]
    }

    paypal.payment.create(createPaymentJson, function (error, payment) {
      if (error) {
        throw error
      } else {
        console.log('Create Payment Response')
        res.redirect(payment.links[1].href)
      }
    })
  }
}
