const express = require('express')
const engines = require('consolidate')
const paypal = require('./paypal')
const paymentController = require('./controllers/paymentController')
const app = express()

app.engine('ejs', engines.ejs)
app.set('views', './views')
app.set('view engine', 'ejs')

app.use(express.json())

app.get('/', (req, res) => {
  res.render('index')
})

const port = process.env.PORT || 3000

app.get('/paypal/:order_id', paymentController.payment)

app.get('/success/:total', (req, res) => {
  // res.send("Success");
  const PayerID = req.query.PayerID
  const paymentId = req.query.paymentId
  const executePaymentJson = {
    payer_id: PayerID,
    transactions: [
      {
        amount: {
          currency: 'BRL',
          total: req.params.total
        }
      }
    ]
  }

  paypal.payment.execute(paymentId, executePaymentJson, function (
    error,
    payment
  ) {
    if (error) {
      throw error
    } else {
      res.render('success')
    }
  })
})

app.get('cancel', (req, res) => {
  res.render('cancel')
})

app.listen(port, () => {
  console.log('Server is running')
})
